/* Follow-up from https://gist.github.com/joepie91/211c8e99fb5a83b76079, which shows promise flattening */

Promise.try(function(){
	return outerThingOne();
}).then(function(value){
	return Promise.try(function(){
		return innerThingOne();
	}).then(function(subValue){
		/* We're using both `value` AND `subValue` here! Both are in scope, because we're nesting. */
		return innerThingTwo(value, subValue);
	});
}).then(function(result){
	return outerThingThree(result);
});